#!/usr/bin/python

# The make_emperor command on Debian lacks a .py extension
#exec make_emperor "$@"

# But then if I use a standard shell wrapper I get complaints that the file
# with a .py extension is not a Python file, so here's a Python equivalent.

import os, sys
os.execvp("make_emperor", ["make_emperor"] + sys.argv[1:])
