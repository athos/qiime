# Startup script for BASH with Qiime environment.
# Source the user's default environent.
test -e "/etc/bash.bashrc" && source "/etc/bash.bashrc"
test -e "~/.bashrc" && source "~/.bashrc"

alias help='man qiime'

source /usr/lib/qiime/shell/qiime_environment
PS1="$PS1 qiime > "
